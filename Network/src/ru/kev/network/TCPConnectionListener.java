package ru.kev.network;

import java.io.IOException;

/**
 * @author Kotelnikova E.V. group 15it20
 */
public interface TCPConnectionListener {

    void onConnectionReady(TCPConnection tcpConnection);

    void onReceiveString(TCPConnection tcpConnection, String s);

    void onDisconnect(TCPConnection tcpConnection);

    void onException(TCPConnection tcpConnection, IOException e);
}
